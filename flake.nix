{
  description = "Rust Development Overlay";

  inputs = {
    nixpkgs.url      = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url  = "github:numtide/flake-utils";
 };

  outputs = { self, nixpkgs, flake-utils, ... }@inputs:
    flake-utils.lib.eachDefaultSystem (system:
      let
        overlays = [  ];
        pkgs = import nixpkgs {
          inherit system overlays;
        };
 
        buildInputs = with pkgs; [
          openssl
          pkg-config
          fd

          # rust
          cargo
          rustc
        ];

        nativeBuildInputs = with pkgs; [  ];
        ldLibPath = pkgs.lib.makeLibraryPath buildInputs;
      in
        {
          devShell = pkgs.mkShell {
            buildInputs = buildInputs ++ [ pkgs.rust-analyzer pkgs.rustfmt pkgs.clippy ];
            nativeBuildInputs = nativeBuildInputs;

            shellHook = ''
            echo "Loaded devshell"
          '';
          };
        }
    );
}
