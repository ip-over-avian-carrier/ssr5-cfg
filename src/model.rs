use std::{borrow::Cow, fmt};

pub const COLOR_ZONES: usize = 10;
pub const DEVICE_NAME: &str = "SteelSeries Rival 5 Destiny 2 Edition";

pub type DeviceResult = std::io::Result<()>;

#[derive(Debug, Clone, Copy)]
pub struct Color {
    pub r: u8,
    pub g: u8,
    pub b: u8,
}

impl From<csscolorparser::Color> for Color {
    fn from(value: csscolorparser::Color) -> Self {
        Self {
            r: (255.0 * value.r).floor() as u8,
            g: (255.0 * value.g).floor() as u8,
            b: (255.0 * value.b).floor() as u8,
        }
    }
}

impl From<&csscolorparser::Color> for Color {
    fn from(value: &csscolorparser::Color) -> Self {
        Self::from(value.clone())
    }
}

impl Color {
    pub fn rgb(r: u8, g: u8, b: u8) -> Self {
        Color { r, g, b }
    }
}

impl fmt::Display for Color {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "#{:02X}{:02X}{:02X}", self.r, self.g, self.b)
    }
}

#[allow(dead_code)]
#[derive(Debug, Clone, Copy)]
#[repr(u8)]
pub enum DeviceOp {
    Save = 0x11,
    LiveLighting = 0x21,
    SoftwareClose = 0x22,
    Brightness = 0x23,
    ReactiveLighting = 0x26,
    ActiveLightingState = 0x27,
    PollingRate = 0x2b,
    DpiLevel = 0x2d,
    SoftwareOpen = 0x90,
}

#[allow(dead_code)]
#[derive(Debug, Clone, Copy)]
#[repr(u8)]
pub enum Brightness {
    Off = 0x00,
    Brightness1 = 0x0c,
    Brightness2 = 0x19,
    Brightness3 = 0x32,
    Brightness4 = 0x64,
}

#[allow(dead_code)]
#[derive(Debug, Clone, Copy)]
#[repr(u8)]
pub enum PollingRate {
    Hz1000 = 0x01,
    Hz500 = 0x02,
    Hz250 = 0x03,
    Hz100 = 0x04,
}

pub struct Device<F>
where
    F: std::io::Write,
{
    handle: F,
}

#[allow(dead_code)]
impl<F> Device<F>
where
    F: std::io::Write,
{
    pub fn new(f: F) -> Self {
        Self { handle: f }
    }

    pub fn save(&mut self) -> DeviceResult {
        println!("** signaling save");
        self.handle.write_all(&[DeviceOp::Save as u8, 0x00])
    }

    pub fn live_lighting(&mut self, colors: [Color; COLOR_ZONES]) -> DeviceResult {
        println!("** setting live lighting zones (total = {}):", COLOR_ZONES);
        for (idx, z) in colors.iter().enumerate() {
            println!("**     {:02}: {}", idx, z)
        }

        let mut buf = Vec::with_capacity(3 + (COLOR_ZONES * 3));
        buf.push(DeviceOp::LiveLighting as u8);
        buf.extend_from_slice(&[0xff, 0x03]);
        for zone in colors {
            buf.extend_from_slice(&[zone.r, zone.g, zone.b])
        }

        self.handle.write_all(&buf)
    }

    pub fn software_close(&mut self) -> DeviceResult {
        println!("** signaling software close");
        self.handle
            .write_all(&[DeviceOp::SoftwareClose as u8, 0xff, 0x03])
    }

    pub fn brightness(&mut self, b: Brightness) -> DeviceResult {
        println!("** setting brightness to: {:?}", b);
        self.handle
            .write_all(&[DeviceOp::Brightness as u8, b as u8])
    }

    pub fn reactive_lighting(&mut self, enable: bool, random: bool, c: Color) -> DeviceResult {
        println!(
            "** setting reactive lighting to: {} ({})",
            if enable { "on" } else { "off" },
            if random {
                Cow::Borrowed("random")
            } else {
                Cow::Owned(format!("color = {}", c))
            }
        );

        self.handle.write_all(&[
            DeviceOp::ReactiveLighting as u8,
            if enable { 0x01 } else { 0x00 },
            if random { 0x01 } else { 0x00 },
            c.r,
            c.g,
            c.b,
        ])
    }

    pub fn active_lighting_state(&mut self, enable: bool) -> DeviceResult {
        println!(
            "** setting lighting state to: {}",
            if enable { "on" } else { "off" }
        );
        self.handle.write_all(&[
            DeviceOp::ActiveLightingState as u8,
            if enable { 0x01 } else { 0x00 },
        ])
    }

    pub fn polling_rate(&mut self, rate: PollingRate) -> DeviceResult {
        println!("** setting polling rate to: {:?}", rate);
        self.handle
            .write_all(&[DeviceOp::PollingRate as u8, rate as u8])
    }

    pub fn dpi(&mut self) -> DeviceResult {
        println!("** setting dpi");
        unimplemented!("cant be fucked");
    }

    pub fn software_open(&mut self) -> DeviceResult {
        println!("** signaling software open");
        self.handle.write_all(&[DeviceOp::SoftwareOpen as u8])
    }
}
