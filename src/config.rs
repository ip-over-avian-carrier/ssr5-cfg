use std::{
    collections::HashMap,
    fs, io,
    path::{Path, PathBuf},
};

use csscolorparser::Color;

#[derive(Debug, Clone, serde::Deserialize)]
pub struct Config {
    pub commit: bool,
    pub lights: HashMap<String, Profile>,
}

#[derive(Debug, Clone, serde::Deserialize)]
pub struct Profile {
    pub brightness: u8,
    pub live: LiveLighting,
    pub reactive: ReactiveLighting,
}

#[derive(Debug, Clone, serde::Deserialize)]
pub struct LiveLighting {
    pub enable: bool,
    pub mwheel: Color,
    pub logo: Color,
    pub left: [Color; 4],
    pub right: [Color; 4],
}

#[derive(Debug, Clone, serde::Deserialize)]
pub struct ReactiveLighting {
    pub enable: bool,
    pub random: bool,
    pub color: Color,
}

fn read_exists(p: impl AsRef<Path>) -> Result<Option<String>, String> {
    match fs::read_to_string(p.as_ref()) {
        Ok(v) => Ok(Some(v)),
        Err(err) => match err.kind() {
            io::ErrorKind::NotFound =>  Ok(None),
            _ =>  Err(format!("unable to open file: {err:?}")),
        },
    }
}

pub fn load() -> Result<Config, String> {
    let p1 = std::env::var("XDG_CONFIG_DIR")
        .map(PathBuf::from)
        .or_else(|_| {
            std::env::var("HOME")
                .map(PathBuf::from)
                .map(|v| v.join(".config"))
        })
        .map_err(|e| format!("unable to discover config dir: {e}"))?
        .join("ssr5")
        .join("config.toml");

    if let Some(data) = read_exists(&p1)? {
        let cfg =
            toml::from_str(&data).map_err(|e| format!("unable to deserialize config: {e}"))?;
        return Ok(cfg);
    }

    let p2 = std::env::var("HOME")
        .map(PathBuf::from)
        .map(|v| v.join(".ssr5-config.toml"))
        .map_err(|e| format!("unable to discover config dir: {e}"))?;

    if let Some(data) = read_exists(&p2)? {
        let cfg =
            toml::from_str(&data).map_err(|e| format!("unable to deserialize config: {e}"))?;
        return Ok(cfg);
    }

    let p3 = PathBuf::from(".").join("ssr5-config.toml");
    if let Some(data) = read_exists(&p3)? {
        let cfg =
            toml::from_str(&data).map_err(|e| format!("unable to deserialize config: {e}"))?;
        return Ok(cfg);
    }

    Err(String::from("no config found"))
}
