use std::fs;
use std::io::BufRead;
use std::io::BufReader;
use std::path::Path;

use model::Brightness;
use model::Color;
use model::Device;
use model::DEVICE_NAME;

mod config;
mod model;

fn find_ss_hidraw() -> Option<String> {
    let mut devs: Vec<_> = fs::read_dir("/dev").ok()?.filter_map(|x| x.ok()).collect();

    devs.sort_by_key(|k| k.path());

    for dev in devs {
        let fname = dev.file_name();
        let name = fname.to_str()?;

        if !name.starts_with("hidraw") {
            continue;
        }

        let sysf_path = Path::new("/sys/class/hidraw")
            .join(name)
            .join("device/uevent");

        let sysf = match fs::File::open(&sysf_path) {
            Ok(f) => BufReader::new(f),
            Err(why) => {
                eprintln!(
                    "!! failed to open hidraw device {}: {}",
                    sysf_path.display(),
                    why
                );
                return None;
            }
        };

        let mut hid_phys = None;
        let mut hid_name = None;

        for line in sysf.lines() {
            let line = match line {
                Ok(_line) => _line,
                Err(_why) => {
                    eprintln!("!! failed to read line of hidfile: {}", _why);
                    return None;
                }
            };

            if line.starts_with("HID_PHYS=") {
                let physx = line.splitn(2, '=').last()?;
                hid_phys = Some(physx.to_owned());
            } else if line.starts_with("HID_NAME=") {
                let namex = line.splitn(2, '=').last()?;
                hid_name = Some(namex.to_owned());
            }
        }

        if hid_phys.is_none() || hid_name.is_none() {
            eprintln!(
                "!! hid device has invalid state, ignoring. read values:\n||     phys = {:?}\n||     name = {:?}",
                hid_phys,
                hid_name
            );

            continue;
        }

        let hid_name = hid_name.unwrap();
        let hid_phys = hid_phys.unwrap();

        if hid_name != DEVICE_NAME {
            continue;
        }

        eprintln!(
            "** found wanted hidraw device:\n**     phys = {}\n**     name = {}",
            hid_phys, hid_name
        );

        return Some(name.to_owned());
    }

    None
}

struct WWrap<F>
where
    F: std::io::Write,
{
    h: F,
}

impl<F> WWrap<F>
where
    F: std::io::Write,
{
    fn new(f: F) -> Self {
        Self { h: f }
    }
}

impl<F> std::io::Write for WWrap<F>
where
    F: std::io::Write,
{
    fn write_all(&mut self, buf: &[u8]) -> std::io::Result<()> {
        println!("WW: {:02X?}", buf);
        self.h.write_all(buf)
    }

    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
        self.h.write(buf)
    }

    fn flush(&mut self) -> std::io::Result<()> {
        self.h.flush()
    }
}

fn main() -> Result<(), String> {
    let config = config::load()?;

    let target_profile = std::env::args().nth(1).ok_or_else(|| {
        format!(
            "usage: {} {{profile}}",
            std::env::args()
                .next()
                .unwrap_or(env!("CARGO_PKG_NAME").to_owned())
        )
    })?;

    let profile = match config.lights.get(&target_profile) {
        Some(p) => p,
        None => return Err(format!("unable to find profile `{}`", target_profile)),
    };

    let hidraw = match find_ss_hidraw() {
        Some(dev) => dev,
        None => {
            return Err(format!(
                "!! unable to find appropriate hidraw dev for name: {}",
                DEVICE_NAME
            ))
        }
    };

    let dev_path = Path::new("/dev").join(hidraw);

    eprintln!("** using dev {} for data", dev_path.display());

    let devf = fs::OpenOptions::new()
        .read(false)
        .append(true)
        .open(dev_path)
        .map_err(|e| format!("!! failed to open hidev device for writing: {}", e))?;

    let mut device = Device::new(WWrap::new(devf));

    device
        .brightness(match profile.brightness {
            0 => Brightness::Off,
            1 => Brightness::Brightness1,
            2 => Brightness::Brightness2,
            3 => Brightness::Brightness3,
            4 => Brightness::Brightness4,
            _ => Brightness::Brightness4,
        })
        .map_err(|e| format!("!! failed to set brightness: {}", e))?;

    if profile.live.enable {
        device
            .live_lighting([
                Color::from(&profile.live.mwheel),
                Color::from(&profile.live.left[0]),
                Color::from(&profile.live.right[0]),
                Color::from(&profile.live.left[1]),
                Color::from(&profile.live.right[1]),
                Color::from(&profile.live.left[2]),
                Color::from(&profile.live.right[2]),
                Color::from(&profile.live.left[3]),
                Color::from(&profile.live.right[3]),
                Color::from(&profile.live.logo),
            ])
            .map_err(|e| format!("!! failed to set lighting: {}", e))?;
    }

    device
        .reactive_lighting(
            profile.reactive.enable,
            profile.reactive.random,
            Color::from(&profile.reactive.color),
        )
        .map_err(|e| format!("!! failed to set brightness: {}", e))?;

    if config.commit {
        device
            .save()
            .map_err(|e| format!("!! failed to save settings: {}", e))?;
    }

    Ok(())
}
